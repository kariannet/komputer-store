// Bank section elements:
const balanceElem = document.getElementById("balance");
const loanButton = document.getElementById("getLoan");
const loanElem = document.getElementById("loan");
// Work section elements:
const payElem = document.getElementById("pay");
const bankButton = document.getElementById("bank");
const workButton = document.getElementById("work");
const repayLoanElem = document.getElementById("repayLoan");
let repayLoanButton;
// Laptop selection section elements:
const laptopsSelect = document.getElementById("laptops");
const laptopFeaturesElem = document.getElementById("laptopFeatures");
// Laptop description section elements:
const laptopImageElem = document.getElementById("laptopImage");
const laptopNameElem = document.getElementById("laptopName");
const laptopDescriptionElem = document.getElementById("laptopDes");
const laptopPriceElem = document.getElementById("laptopPrice");
const buyButton = document.getElementById("buy");

let balance;
let pay;
let loan = 0.0;

let selectedLaptop;
let laptops = [];

const imageElem = document.createElement("img");

const path = "https://noroff-komputer-store-api.herokuapp.com/";

fetch(path + "computers")
.then(response => response.json())
.then(data => laptops = data)
.then(laptops => addLaptopsToMenu(laptops));

/**
 * Clears the content of the given HTML element.
 * @param {*} element 
 */
const clearContent = (element) => {
    element.innerText = "";
};

/**
 * 
 * @param {*} number 
 * @returns the given number in Norwegian currency format.
 */
const changeToCurrencyFormat = (number) => (
    new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(number)
    );
    /**
     * Adds the given list of objects to the select menu HTML element 
     * LaptopsSelect.
 * @param {*} laptops 
     */
    const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    // Update display and selectedLaptop:
    selectedLaptop = laptops[0];
    updateLaptop();
};

/**
 * Adds the given object to the select menu HTML element, laptopsSelect,
 * diplaying its title.
 * @param {*} laptop 
 */
const addLaptopToMenu = (laptop) => {
    const laptopElem = document.createElement("option");
    laptopElem.value = laptop.id;
    laptopElem.appendChild(document.createTextNode(laptop.title));
    laptopsSelect.appendChild(laptopElem);
};

/**
 * Updates display to the selected laptop.
 */
const updateLaptop = () => {
    displayLaptopSpecs(selectedLaptop.specs);
    displayLaptopInfo(selectedLaptop);
};

/**
 * Updates the HTML element laptopFeaturesElem to list all of the 
 * given laptop specs.
 * @param {*} laptopSpecs 
 */
const displayLaptopSpecs = (laptopSpecs) => {
    clearContent(laptopFeaturesElem);
    laptopSpecs.forEach(x => {
        const specElem = document.createElement("li");
        specElem.innerText = x;
        laptopFeaturesElem.appendChild(specElem);
    });
};

/**
 * Updates the HTML elements laptopImageElem, laptopNameElem, 
 * laptopDescriptionElem and laptopPriceElem to display the
 * wanted attributes (name, description, image, price) of the 
 * given laptop.
 * @param {*} laptop 
 */
const displayLaptopInfo = (laptop) => {
    // displaying the image
    clearContent(laptopImageElem); // clearing former image
    imageElem.src = path + laptop.image;
    imageElem.alt = laptop.title;
    laptopImageElem.appendChild(imageElem);
    // displaying laptop title
    laptopNameElem.innerText = laptop.title;
    // displaying laptop description
    laptopDescriptionElem.innerText = laptop.description;
    // displaying laptop price
    laptopPriceElem.innerText = changeToCurrencyFormat(laptop.price);
};

/**
 * Updates and displays the bank balance.
 * @param {*} newBalance 
 */
const updateBankBalance = (newBalance) => {
    balance = newBalance;
    balanceElem.innerText = changeToCurrencyFormat(balance);
};

/**
 * Updates and displays the pay balance.
 * @param {*} newPay 
 */
const updateWorkPay = (newPay) => {
    pay = newPay;
    payElem.innerText = changeToCurrencyFormat(pay);
};

/**
 * Updates and displays loan.
 * @param {*} newLoan 
 */
const updateLoan = (newLoan) => {
    loan = newLoan;
    loanElem.innerText = `Outstanding loan: ${changeToCurrencyFormat(loan)}`;
    // If loan has been repaid, remove the "repay loan" button:
    if (loan === 0.0) {
        clearContent(repayLoanElem);
    }
};

/**
 * Transfer the given amount to loan (reduces outstanding loan).
 * If the given amount is greater than the outstanding loan, the residue 
 * is transferred to the bank balance.
 * @param {*} amount 
 */
const transferToLoanAccount = (amount) => {
    if (amount > loan) {
        const forBank = balance + amount - loan;
        updateBankBalance(forBank);
        updateLoan(0.0);
    }
    else {
        updateLoan(loan - amount);
    }
};

/**
 * Handles clicking of the "Get Loan" button. 
 */
const handleGetLoan = () => {
    if (loan > 0) {
        alert("You can't get a new loan before you have repaid your old!");
    }
    else {
        const loanAmount = parseFloat(prompt("Please enter the amount of money you wish to loan. The amount can not be more than double of your bank balance: "));
        if (!loanAmount) {
            alert("You didn't enter an amount!");
        }
        else if (loanAmount > balance*2) {
            alert("Your loan can't be greater than double of your bank balance!");
        }
        else {
            updateBankBalance(balance+loanAmount);
            updateLoan(loanAmount);
            // pop-up repay loan button
            repayLoanButton = document.createElement("button");
            repayLoanButton.id = "repayLoanButton";
            repayLoanButton.innerText = "Repay Loan";
            repayLoanElem.appendChild(repayLoanButton);
            repayLoanButton.addEventListener("click", handleRepayLoan);
        }
    }
};

/**
 * Handles changing laptop in the select menu.
 * @param {*} e 
 */
const handleLaptopMenuChange = (e) => {
    // Update selected laptop:
    selectedLaptop = laptops[e.target.selectedIndex];
    // Change display:
    updateLaptop();
};

/**
 * Handles clicking of the "Buy now" button.
 */
const handleBuyLaptop = () => {
    if (balance < selectedLaptop.price) {
        alert("Insufficient bank balance!");
    }
    else {
        updateBankBalance(balance-selectedLaptop.price);
        alert(`You have bought the ${selectedLaptop.title}.`);
    }
};

/**
 * Handles clicking of the "Repay loan" button.
 */
const handleRepayLoan = () => {
    transferToLoanAccount(pay);
    updateWorkPay(0.0);
};

/**
 * Handles clicking of the "Work" button.
 */
const handleWork = () => {
    updateWorkPay(pay+100.0);
};

/**
 * Handles clicking of the "Bank" button.
 */
const handleBank = () => {
    if (loan > 0) {
        const loanAmount = pay*0.1;
        transferToLoanAccount(loanAmount);
        updateBankBalance(balance+pay-loanAmount);
    }
    else {
        updateBankBalance(balance+pay);
    }
    updateWorkPay(0.0);
};

/**
 * Handles if image has wrong path.
 */
const handleImageError = () => {
    const newSourceArray = imageElem.src.split(".");
    newSourceArray.pop();
    newSourceArray.push("png");
    imageElem.src = newSourceArray.join(".");
}


// Initializing:
updateBankBalance(0.0);
updateWorkPay(0.0);

imageElem.addEventListener("error", handleImageError);
laptopsSelect.addEventListener("change", handleLaptopMenuChange);
buyButton.addEventListener("click", handleBuyLaptop);
loanButton.addEventListener("click", handleGetLoan);
workButton.addEventListener("click", handleWork);
bankButton.addEventListener("click", handleBank);

