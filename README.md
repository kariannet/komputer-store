# Komputer Store

## Description
The Komputer Store is an imaginary online store for buying computers where the user have their own bank and work salary accounts - all in one webpage. It has been built using plain JavaScript with no frameworks.

#### The Bank Section
The bank section displays you account balance (the amount available to buy a laptop) as well as your outstanding loan (if you have one). Clicking the button "Get a loan" allows you to enter the amount you wish to loan, however, this amount can't be more than double of your bank balance.

### The Work Section
This section displays the amount of money you have earned through "work". Clicking the "Work" button will increase your pay with 100 kr. Clicking the "Bank" button will transfer your pay balance to your bank balance. If you have a loan, 10% of your pay will go to paying off your loan. In addition, the "Repay Loan" button will show up in this section. Clicking this button will transfer all of your pay balance to pay off your outstanding loan, any remaining funds will be tranferred to your bank account.

### The Laptop Selection Section
In the select box you can choose between the available laptops. Features of the selected laptop will be displayed below.

### The Laptop Information Section
The image, name, description and price of the selected laptop is displayed here. By clicking the "BUY NOW" button, you can buy the selected laptop if you have enough in your bank account to do so.

## Maintainers
Karianne Tesaker - @kariannet

## Usage
Clone the repository and open html file in e.g. your favourite browser to use the webpage. Open the code files in your favourite text editor to make any changes.

## Visuals

### Wireframe
![wireframe](/wireframe/wireframe.png "Komputer Store Wireframe")

## Project status
Completed.

